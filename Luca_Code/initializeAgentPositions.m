function [ Attacker, Defender ] = initializeAgentPositions()
    %posX,posY,type,Id,name,taskAssignment,listTask,listSendMess,listReceivedMess
	%attacker1 = Agent(9,3,1,1,'Predator1',false,[],[],[]);
    attacker1 = Agent(1,1,1,1,'Predator1',false,[],[],[]);
    %attacker2 = Agent(1,6,1,2,'Predator2',false,[],[],[]);
    attacker2 = Agent(7,4,1,2,'Predator2',false,[],[],[]);
%	attacker3 = Agent(4,5,1,3,'Predator3',false,[],[],[]);
    attacker3 = Agent(9,6,1,3,'Predator3',false,[],[],[]);
	attacker4 = Agent(7,9,1,4,'Predator4',false,[],[],[]);
	attacker5 = Agent(2,8,1,5,'Predator5',false,[],[],[]);
	
	defender1 = Agent(1,4,2,1,'Prey1',false,[],[],[]);
	defender2 = Agent(6,3,2,2,'Prey2',false,[],[],[]);
	defender3 = Agent(9,2,2,3,'Prey3',false,[],[],[]);
	defender4 = Agent(3,4,2,4,'Prey4',false,[],[],[]);
	defender5 = Agent(3,6,2,5,'Prey5',false,[],[],[]);
	
	Attacker = [ attacker1, attacker2, attacker3, attacker4, attacker5 ];
	Defender = [ defender1, defender2, defender3, defender4, defender5 ];
end
