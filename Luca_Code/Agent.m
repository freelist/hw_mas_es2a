classdef Agent
	properties
		POS_X
		POS_Y
        %predatore o preda
        type
        id
        name
        %variabile che assegna il task true o false
        taskAssigned
        %il numero di task da assegnare
        tasks
        %buffer Send Messages
        outMessages
        %buffer Received Messages
        inMessages
	end
	
	methods
		function obj = Agent(POS_X, POS_Y, TYPE, ID, NAME, TASKASSIGNED, TASKS, OUTMESSAGES, INMESSAGES)
			obj.POS_X = POS_X;
			obj.POS_Y = POS_Y;
            obj.type = TYPE;
            obj.id = ID;
            obj.name = NAME;
            obj.taskAssigned = TASKASSIGNED;
            obj.tasks = TASKS;
            obj.outMessages = OUTMESSAGES;
            obj.inMessages= INMESSAGES;
        end
        
        
		
		function [ Res, obj, worldObj, Res2, DetectedPreysId ] = move(obj, worldObj, X, Y)
            [ Res, worldObj ] = worldObj.tryMove(obj.POS_X,obj.POS_Y,X,Y);
          
            if Res == 1
                obj.POS_X = X;
                obj.POS_Y = Y;
            end

            [Res2, DetectedPreysId] = obj.detectPrey(worldObj);
            if Res2==1
                for i=1:length(DetectedPreysId)
                    display([obj.name,' detected the ',worldObj.Defender(DetectedPreysId(i)).name,' at cell(',num2str(worldObj.Defender(DetectedPreysId(i)).POS_X),',',num2str(worldObj.Defender(DetectedPreysId(i)).POS_Y),')']);
                end
                
                
            end
        end
        
        % sceglie random il goal nearest la preda
        function [goal_x, goal_y] = calculateGoal(obj, worldObj)
            prey_x = worldObj.Defender(obj.tasks(end)).POS_X;
            prey_y = worldObj.Defender(obj.tasks(end)).POS_Y;
            cellUp = [prey_x-1,prey_y]; 
            if ((cellUp(1,1)<1 ||cellUp(1,2)<1) || (cellUp(1,1)>worldObj.DIM_X || cellUp(1,2)>worldObj.DIM_Y))
                cellUp = [0,0];
            end
            cellDown = [prey_x+1,prey_y];
            if ((cellDown(1,1)<1 ||cellDown(1,2)<1) || (cellDown(1,1)>worldObj.DIM_X || cellDown(1,2)>worldObj.DIM_Y))
                cellDown = [0,0];
            end
            cellLeft = [prey_x,prey_y-1];
            if ((cellLeft(1,1)<1 ||cellLeft(1,2)<1) || (cellLeft(1,1)>worldObj.DIM_X || cellLeft(1,2)>worldObj.DIM_Y))
                cellLeft = [0,0];
            end
            cellRight = [prey_x,prey_y+1];
            if ((cellRight(1,1)<1 ||cellRight(1,2)<1) || (cellRight(1,1)>worldObj.DIM_X || cellRight(1,2)>worldObj.DIM_Y))
                cellRight = [0,0];
            end
            cellUpLeft = [prey_x-1,prey_y-1];
            if ((cellUpLeft(1,1)<1 ||cellUpLeft(1,2)<1) || (cellUpLeft(1,1)>worldObj.DIM_X || cellUpLeft(1,2)>worldObj.DIM_Y))
                cellUpLeft = [0,0];
            end
            cellUpRight = [prey_x-1,prey_y+1];
            if ((cellUpRight(1,1)<1 ||cellUpRight(1,2)<1) || (cellUpRight(1,1)>worldObj.DIM_X || cellUpRight(1,2)>worldObj.DIM_Y))
                cellUpRight = [0,0];
            end
            cellBottomLeft = [prey_x+1,prey_y-1];
            if ((cellBottomLeft(1,1)<1 ||cellBottomLeft(1,2)<1) || (cellBottomLeft(1,1)>worldObj.DIM_X || cellBottomLeft(1,2)>worldObj.DIM_Y))
                cellBottomLeft = [0,0];
            end
            cellBottomRight = [prey_x+1,prey_y+1];
            if ((cellBottomRight(1,1)<1 ||cellBottomRight(1,2)<1) || (cellBottomRight(1,1)>worldObj.DIM_X || cellBottomRight(1,2)>worldObj.DIM_Y))
                cellBottomRight = [0,0];
            end
            adiacentCells = [cellUp;cellDown;cellLeft;cellRight;cellUpLeft;cellUpRight;cellBottomLeft;cellBottomRight];
            realAdiacentCells = [];
            ctr=1;
            for i=1:8
                if adiacentCells(i,1)~=0 && adiacentCells(i,2)~=0
                    realAdiacentCells(ctr) = adiacentCells(i);
                    ctr = ctr+1;
                end
            end
            r = randi([1,length(realAdiacentCells)]);
            goal_x = adiacentCells(r,1);
            goal_y = adiacentCells(r,2);
        end
        
        function [d] = calculateD(obj, goal_x, goal_y)
            if (abs(obj.POS_X-goal_x)<=1 && abs(obj.POS_Y-goal_y)<=1)
                d = 1;
            else
                d = 0;
            end
        end
        
        %la prossima posizione del prossimo step dell'agente x,y
        %moveflag abilita il movimento del predatore con il task assegnato
        function [res_x, res_y, moveFlag] = nextStep(obj, worldObj)
            moveFlag = true;
            if obj.taskAssigned
                [goal_x, goal_y] = obj.calculateGoal(worldObj);
                md = obj.calculateD(worldObj.Defender(obj.tasks(end)).POS_X,worldObj.Defender(obj.tasks(end)).POS_Y);
                if md == 1
                    moveFlag = false;
                    res_x = goal_x;
                    res_y = goal_y;
                else
                    goal_x_reached = false;
                    goal_y_reached = false;
                    %decide where to move up/down or right/left
                    if obj.POS_X == goal_x
                        goal_x_reached = true;
                    end
                    if obj.POS_Y == goal_y
                        goal_y_reached = true;
                    end
                    if goal_x_reached
                        movementChoice = 2;
                    elseif goal_y_reached
                        movementChoice = 1;
                    else
                        movementChoice = randi([1,2]);
                    end
                    if movementChoice == 1
                        % vai giu
                        if (goal_x - obj.POS_X) > 0
                            NEW_X = obj.POS_X + 1;
                            NEW_Y = obj.POS_Y;
                            % check boundary
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                %vado a dx
                                if (goal_y - obj.POS_Y) > 0
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y + 1;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 - 2;
                                        if ((res_x<1 ||res_y<1) || (res_x>worldObj.DIM_X || res_y>worldObj.DIM_Y))
                                            res_x = NEW_X2;
                                            res_y = NEW_Y2 - 1;
                                        else
                                            res_x = NEW_X2;
                                            res_y = NEW_Y2 - 2;
                                        end
                                    end
                                % vado a sx    
                                else
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y - 1;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 + 1;
                                    end
                                end
                            end
                        % vai su
                        else
                            NEW_X = obj.POS_X - 1;
                            NEW_Y = obj.POS_Y;
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                % vado a dx
                                if (goal_y - obj.POS_Y) > 0
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y + 1;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 - 1;
                                    end
                                % vado a sx
                                else
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y - 1;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 + 1;
                                    end
                                end
                            end
                        end
                    elseif movementChoice == 2
                        % vai a dx
                        if (goal_y - obj.POS_Y) > 0
                            NEW_X = obj.POS_X;
                            NEW_Y = obj.POS_Y + 1;
                            % check boundary
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                %vado su
                                if (goal_x - obj.POS_X) > 0
                                    NEW_X2 = obj.POS_X - 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 + 2;
                                        res_y = NEW_Y2;
                                        if ((res_x<1 ||res_y<1) || (res_x>worldObj.DIM_X || res_y>worldObj.DIM_Y))
                                            res_x = NEW_X2 + 1;
                                            res_y = NEW_Y2;
                                        else
                                            res_x = NEW_X2 + 2;
                                            res_y = NEW_Y2;
                                        end
                                    end
                                % vado giu    
                                else
                                    NEW_X2 = obj.POS_X + 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 - 1;
                                        res_y = NEW_Y2;
                                    end
                                end
                            end
                        % vado a sx
                        else
                            NEW_X = obj.POS_X;
                            NEW_Y = obj.POS_Y - 1;
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                % vado giu
                                if (goal_x - obj.POS_X) > 0
                                    NEW_X2 = obj.POS_X + 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 - 1;
                                        res_y = NEW_Y2;
                                    end
                                % vado su
                                else
                                    NEW_X2 = obj.POS_X - 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 + 1;
                                        res_y = NEW_Y2;
                                    end
                                end
                            end
                        end
                    end
                end
            else
                %muoviti random
                movementChoice = randi([1,2]);
                if movementChoice == 1
                     res_x = randi(worldObj.DIM_X);
                     res_y = obj.POS_Y;
                else
                res_x = obj.POS_X;
                res_y = randi(worldObj.DIM_Y);
                end
            end
        end
        
        %function che verifica se intorno a me c'� una preda
        %DetectedPreyes contiene l'id della preda
        function [Res, DetectedPreysId] = detectPrey(obj, worldObj)
            Res = 0;
            DetectedPreysId = [];
            if obj.taskAssigned == false
            
            counter = 1;
            Defender = worldObj.Defender;
            cellUp = [obj.POS_X-1,obj.POS_Y]; 
            cellDown = [obj.POS_X+1,obj.POS_Y];
            cellLeft = [obj.POS_X,obj.POS_Y-1];
            cellRight = [obj.POS_X,obj.POS_Y+1];
            cellUpLeft = [obj.POS_X-1,obj.POS_Y-1];
            cellUpRight = [obj.POS_X-1,obj.POS_Y+1];
            cellBottomLeft = [obj.POS_X+1,obj.POS_Y-1];
            cellBottomRight = [obj.POS_X+1,obj.POS_Y+1];
            for i=1:length(Defender)
                if Defender(i).POS_X == cellUp(1,1) && Defender(i).POS_Y == cellUp(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellDown(1,1) && Defender(i).POS_Y == cellDown(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellLeft(1,1) && Defender(i).POS_Y == cellLeft(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;           
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellRight(1,1) && Defender(i).POS_Y == cellRight(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellUpLeft(1,1) && Defender(i).POS_Y == cellUpLeft(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellUpRight(1,1) && Defender(i).POS_Y == cellUpRight(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellBottomLeft(1,1) && Defender(i).POS_Y == cellBottomLeft(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end
                if Defender(i).POS_X == cellBottomRight(1,1) && Defender(i).POS_Y == cellBottomRight(1,2)
                    Res = 1;
                    DetectedPreysId(counter) = Defender(i).id;
                    counter = counter + 1;
                end    
            end  
            else
            end    
        end
        
        function [Cnp] = startCNP(obj, worldObj)
            Attacker = worldObj.Attacker;
            Defender = worldObj.Defender;
            cost=0;
            Cnp=0;
            for i=1:length(Attacker)
                if ((Attacker(i).taskAssigned==false) && (obj.id~=Attacker(i).id))
                    utilFunction= 1-(abs(Attacker(i).POS_X-Defender(obj.tasks(1)).POS_X)+abs(Attacker(i).POS_Y-Defender(obj.tasks(1)).POS_Y))/20;
                    if utilFunction > cost
                        cost=utilFunction;
                        Cnp=Attacker(i).id;
                    end    
                end
            
            end
        end
	end
end
