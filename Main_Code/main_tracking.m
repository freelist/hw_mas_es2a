function main_tracking
	DEBUG = 0;
	
	[ Attacker, Defender ] = initializeAgentPositions;
	
	world = World(10,10);
	
	[ world ] = world.initWorld(Attacker,Defender);
	
	figure('name','World');
	
	world.printWorldState(Attacker,Defender);
    %pause
	
	if DEBUG == 1
		fprintf('\n');
	end
	
	isGameEnded = 0;
    
    stepCtr = 1;
	
	while isGameEnded == 0
        
        display(['************** STEP ',num2str(stepCtr),' **************']);
        stepCtr = stepCtr + 1;
        
		for i = 1:length(Attacker(1,:))
            [r_x, r_y, moveFlag] = Attacker(i).nextStep(world);
			
            if(moveFlag)
                [ Res, Attacker(i), world, DetectedPraysFlag, DetectedPrays ] = Attacker(i).move(world,r_x,r_y);
                if DetectedPraysFlag==1 
                    if Attacker(i).taskAssigned == false
                        for j=1:length(DetectedPrays)
                            Attacker(i).tasks(end+1)=DetectedPrays(j);
                            Attacker(i).taskAssigned=true;
                            world.Attacker(i).tasks(end+1)=DetectedPrays(j);
                            world.Attacker(i).taskAssigned=true;
                            if size(Attacker(i).outMessages,1)==0 && size(Attacker(i).outMessages,2)==0
                                cnp = Attacker(i).startCNP(world,DetectedPrays(j));
                            else
                                if (find(Attacker(i).outMessages(:,1)==DetectedPrays(j))>0)
                                    cnp = 0;
                                else
                                    cnp = Attacker(i).startCNP(world,DetectedPrays(j));
                                end
                            end
                            if cnp ~= 0
                                Attacker(cnp).tasks(end+1)=DetectedPrays(j);     
                                Attacker(cnp).taskAssigned=true;
                                world.Attacker(cnp).tasks(end+1)=DetectedPrays(j);     
                                world.Attacker(cnp).taskAssigned=true;
                                display(['Predator',num2str(i),' assigned Prey',num2str(DetectedPrays(j)),' to Predator',num2str(world.Attacker(cnp).id)]);
                                Attacker(i).outMessages(end+1,:)=[DetectedPrays(j) cnp];
                                world.Attacker(i).outMessages(end+1,:) = [DetectedPrays(j) cnp];
                                %pause
                            end
                            Defender(DetectedPrays(j)).ctrDeath = Defender(DetectedPrays(j)).ctrDeath + 1;
                            world.Defender(DetectedPrays(j)).ctrDeath = world.Defender(DetectedPrays(j)).ctrDeath + 1;
                            display(['ctrDeath preda ',num2str(world.Defender(DetectedPrays(j)).id),': ',num2str(world.Defender(DetectedPrays(j)).ctrDeath)]);
                        end
                    else
                        for j=1:length(DetectedPrays)
                            if(find(Attacker(i).tasks==DetectedPrays(j))>0)
                                Defender(DetectedPrays(j)).ctrDeath = Defender(DetectedPrays(j)).ctrDeath + 1;
                                world.Defender(DetectedPrays(j)).ctrDeath = world.Defender(DetectedPrays(j)).ctrDeath + 1;
                            else
                                if size(Attacker(i).outMessages,1)==0 && size(Attacker(i).outMessages,2)==0
                                    cnp = Attacker(i).startCNP(world,DetectedPrays(j));
                                else
                                    if (find(Attacker(i).outMessages(:,1)==DetectedPrays(j))>0)
                                        cnp = 0;
                                    else
                                        cnp = Attacker(i).startCNP(world,DetectedPrays(j));
                                    end
                                end
                                if cnp ~= 0
                                    Attacker(cnp).tasks(end+1)=DetectedPrays(j);  
                                    Attacker(cnp).taskAssigned=true;
                                    world.Attacker(cnp).tasks(end+1)=DetectedPrays(j);  
                                    world.Attacker(cnp).taskAssigned=true;
                                    display(['Predator',num2str(i),' assigned Prey',num2str(DetectedPrays(j)),' to Predator',num2str(world.Attacker(cnp).id)]);
                                    Attacker(i).outMessages(end+1,:)=[DetectedPrays(j) cnp];
                                    world.Attacker(i).outMessages(end+1,:)=[DetectedPrays(j) cnp];
                                    pause
                                end
                            end
                        end
                    end
                end
            else
                %if(length(Attacker(i).tasks)-1 ~= 0 )
                %    Attacker(i).tasks = Attacker(i).tasks(1:(end-1));
                %end
                %Res = 1;
            end
			
			if Res == 2
				isGameEnded = 1;
				
				fprintf('\n*************************************************************\n');
				fprintf('GAME ENDED.');
				fprintf('*************************************************************\n\n');
			end
			
			if DEBUG == 1
				fprintf('Attacker %d is now in state: (%d, %d)\n', i, Attacker(i).POS_X, Attacker(i).POS_Y);
			end
        end
        
        for i = 1:length(Defender)
            if Defender(i).isDead ~= true
                if(Defender(i).ctrDeath == 2)
                    for j = 1:length(Attacker)
                        if(find(Attacker(j).tasks==Defender(i).id)>0)
                            if(length(Attacker(j).tasks)-1 ~= 0 )
                                Attacker(j).tasks = Attacker(j).tasks(Attacker(j).tasks~=Defender(i).id);
                                world.Attacker(j).tasks = world.Attacker(j).tasks(world.Attacker(j).tasks~=Defender(i).id);
                            else
                                Attacker(j).tasks = [];
                                world.Attacker(j).tasks = [];
                                Attacker(j).taskAssigned = false;
                                world.Attacker(j).taskAssigned = false;
                            end
                        end
                    end
                    [world, Defender]= world.deletePrey(Defender(i).id,Defender);
                end
            end
        end
		
		if DEBUG == 1
			fprintf('\n');
		end
		
		%for i = 1:length(Defender(1,:))
		%	r_x = randi(world.DIM_X);
		%	r_y = randi(world.DIM_Y);
		%	
		%	[ ~, Defender(i), world ] = Defender(i).move(world,r_x,r_y);
		%	
		%	if DEBUG == 1
		%		fprintf('Defender %d is now in state: (%d, %d)\n', i, Defender(i).POS_X, Defender(i).POS_Y);
		%	end
		%end
		
		if DEBUG == 1
			fprintf('\n');
		end
		
		world.printWorldState(Attacker,Defender);
		
		if DEBUG == 1
			fprintf('\n');
        end
		
        ctrEnd = 0;
        for i=1:length(Defender)
            if Defender(i).isDead
                ctrEnd = ctrEnd + 1;
            end
        end
        
        if ctrEnd == length(Defender)
            isGameEnded = 1;
            fprintf('\n*************************************************************\n');
            fprintf('GAME ENDED.');
            fprintf('*************************************************************\n\n');
        end
        
		pause(1);
	end
end
