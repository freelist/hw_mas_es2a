package multiagent;

/**
* This class reprents a possible action that an Agent can take.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/
public enum Action{
    /**
    * Action performed when the agent has reached the target and it's exploring the surrounding area.
    */
    nextStepInTask,
    /**
    * Action performed when the agent still has to reach the target and it's moving towards it.
    */
    nextMoveToTarget,
    /**
    * No operation is performed if there is no task under execution.
    */
    noOp
}
