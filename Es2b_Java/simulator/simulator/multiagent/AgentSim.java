package multiagent;

import java.io.*;
import java.util.*;

/**
* This class implements a full simulator.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/

public class AgentSim implements Serializable{
                
    /**The agents*/
    public List<Agent> agents;
    /**The world*/
    public World world;
    /**The agent currently executing an action (used for simulation purposes)*/
    private int currentAgent;
    
    /**
    * Counter used to check if the exploration process has ended.
    */
    private int inactiveSteps;
    
    /**Number of noOp Actions performed (used for statistics).*/
    private int totalNoOps;
    
    /**Number of nextMoveToTarget Actions performed (used for statistics).*/
    private int totalNextMoves;
    
    /**Number of nextStepInTask Actions performed (used for statistics).*/
    private int totalNextSteps;
    
    /**
    * Constructs a simulator creating a new world with the specified characteristics.
    * @param height the height of the world
    * @param width the width of the world
    */
    public AgentSim(int height, int width){
        world=new World(height,width);
    }
    
    /**
    * Constructs a simulator creating a new world with the specified characteristics.
    * @param height the height of the world
    * @param width the width of the world
    * @param obs the number of inaccessible cells
    * @param ags the number of agents
    * @param tasks the number of tasks to be initially assigned
    */
    public AgentSim(int height, int width, int obs, int ags, int tasks){
        world=new World(height,width);
        addInaccessibleCells(obs);
        initAgentsRandom(ags);
        initTasks(tasks);
    }
    
    /**
    * Constructs a simulator with a world loaded by a given file.
    * @param filename the name of the file the world is stored in
    */
    public AgentSim(String filename)throws IOException, ClassNotFoundException{
        this.world=World.retrieveWorld(filename);
    }
    
    /**
    * Constructs a simulator with a world loaded by a given file, adding agents and tasks.
    * @param filename the name of the file the world is stored in
    * @param ags the number of agents
    * @param tasks the number of tasks to be initially assigned
    */
    public AgentSim(String filename, int ags, int tasks)throws IOException, ClassNotFoundException{
        this(filename);
        initAgentsRandom(ags);
        initTasks(tasks);
    }
    
    /**
    * Records the grid map of the world in a file with the given name,
    * marking all non-inaccessible Cells as notSeen.
    * @param filename the name of the file the World will be stored
    * @throws IOException
    */
    public void storeWorld(String filename) throws IOException{
        world.storeWorld(filename);
    }
    
    /**
    * Adds inaccessible cells.
    * @param obs the number of inaccessible cells that we want to add
    */
    public void addInaccessibleCells(int obs){
        world.addInaccessibleCells(obs);
    }

    /** Puts the agents in the positions specified.
    * An exception is thrown if there are conflicts with inaccessible locations.
    */
    public void initAgents(Location[]locations) {
        agents=new LinkedList<Agent>();
        for(int i=0;i<locations.length;i++){
            Location loc=locations[i];
            Cell cell=world.getCell(loc);
            if(cell.getStatus()!=Cell.Status.inaccessible)
                agents.add(new Agent(i+1,loc));
            else
                throw new IllegalArgumentException("Location inaccessible");
        }
        world.addAgents(agents);
    }

    /**Positions agents in the world in random positions.*/
    public void initAgentsRandom(int numAgents){
        agents=new LinkedList<Agent>();
        for(int i=1;i<=numAgents;i++){
            Location loc=new Location((int)(world.getHeight()*Math.random()),(int)(world.getWidth()*Math.random()));
            Cell cell=world.getCell(loc);
            if(cell.getStatus()!=Cell.Status.inaccessible)
                agents.add(new Agent(i,loc));
            else
                i--;
        }
        world.addAgents(agents);
    }
    
    /**
    * Random task initialization.
    * It initially assigns task to a number t of agents, if it is possible to
    * do so without generating superimpositions.
    * The Locations of each task are assigned randomly according
    * to the sequential ordering of the agents. If an agent rejects the first
    * task found for him (which shouldn't happen in initialization phase) it
    * is skipped.
    * <p> If, after assigning n<t tasks, there are no more notSeen Cells in the world,
    * it becomes impossible to allocate the requested number of tasks; in such case,
    * the method stops.
    * For external check, the number of tasks actually assigned is returned.
    * <p> Note: since this method, to assign a task, keeps selecting random cells
    * until it finds a notSeen one, it might take a long time to terminate in a very
    * "dense" and populated world.
    * @param t the number of tasks to be assigned.
    * @throws IllegalArgumentException if t is negative or greater than the number of agents.
    * @return the number of tasks actually assigned.
    */
    public int initTasksRandom(int t){
        if(t<0||t>agents.size())
            throw new IllegalArgumentException("Inadmissible number of tasks");
        
        int assignedTasks=0;
        
        for(Iterator<Agent>it=agents.iterator();it.hasNext()&&t>0;t--){
            if(countCells(Cell.Status.notSeen)==0)break;
            Agent ag=it.next();
            
            while(true){
                Location loc=new Location((int)(world.getHeight()*Math.random()),(int)(world.getWidth()*Math.random()));
                if(world.getCell(loc).getStatus()==Cell.Status.notSeen){
                    boolean accepted=ag.acceptTask(loc);
                    if(accepted){
                        world.addTask(loc);
                        assignedTasks++;
                    }
                    else t++;
                    break;
                }
            }
        }
        
        return assignedTasks;
    }
    
    /**
    * Task initialization.
    * It initially assigns task to a number t of agents, if it is possible to
    * do so without generating superimpositions.
    * Agents are assigned a task corresponding to their own initial positions.
    * The Locations of each task are assigned according
    * to the sequential ordering of the agents. If an agent lies within a task
    * which has already been assigned, it is skipped. An agent is also skipped
    * if the agent rejects its task (which shouldn't happen in initialization phase).
    * <p> Many superimpositions (or many refusals) may make it
    * impossible to assign the requested number of tasks with this criterion;
    * in such case, the simulator assigns as many tasks as it can.
    * For external check, the number of tasks actually assigned is returned.
    * @param t the number of tasks to be assigned.
    * @throws IllegalArgumentException if t is negative or greater than the number of agents.
    * @return the number of tasks actually assigned.
    */
    public int initTasks(int t){
        if(t<0||t>agents.size())
            throw new IllegalArgumentException("Inadmissible number of tasks");
        
        int assignedTasks=0;
        
        for(Iterator<Agent>it=agents.iterator();it.hasNext()&&t>0;t--){
            Agent ag=it.next();
            Location loc=ag.getPosition();
            if(world.getCell(loc).getStatus()==Cell.Status.notSeen){  //check needed if initial position falls in an already assigned task.
                boolean accepted=ag.acceptTask(loc);
                if(accepted){
                    world.addTask(loc);
                    assignedTasks++;
                }
                else t++;
            }
            else t++;
        }
        
        return assignedTasks;
    }

    /**
    * Executes one step for one agent and switches agent.
    * <p> At each turn an Agent needs to: 
    * <p>  - specify the next action;
    * <p>  - based on the result of the action returned by the world the agent must update the task list;
    * <p>  - try to allocate all the actions in the list;
    * <p>  - release turn.
    * <p> In addition, each Agent must act as provider when a request for Bid is issued by another agent.
    * @return true if there have been two full rounds without any action taken,
    * indicating that the system will have no further evolution.
    */
    public boolean doOneStep(){
        Agent agent=agents.get(currentAgent);
	Action act=agent.nextAction();
        
        //managing statistics and inactive steps count
        if(act==Action.noOp){
            inactiveSteps++;
            totalNoOps++;
        }
        else{
            inactiveSteps=0;
            
            if(act==Action.nextMoveToTarget)
                totalNextMoves++;
            else
                totalNextSteps++;
        }
        
	Location nextTask=world.executeAction(agent.getId(),agent.getCurrentTask(),act);
        world.addTask(nextTask);
        agent.updateState(world.getAgentPosition(agent.getId()),nextTask);

	agent.assignTasks(agents);
        
        currentAgent=(currentAgent+1)%agents.size();
        
        if(inactiveSteps>=2*agents.size())
            return true;
        else
            return false;
    }
    
    /**
    * Performs a complete simulation.
    */
    public void loop(){
        while(true){
            boolean stop=doOneStep();
            if(stop)return;
        }
    }
    
    //************************************************ Methods to retrieve information about the world and about the simulation ******
    
    /**
    * Returns the number of cells with the given status.
    * @param status the status to examine.
    * @return the number of cells with the given status.
    */
    public int countCells(Cell.Status status){
        int count=0;
        for(int row=world.getHeight()-1;row>=0;row--)
            for(int col=world.getWidth()-1;col>=0;col--)
                if(world.getCell(row,col).getStatus()==status)
                    count++;
        return count;
    }
    
    /**
    * Returns the total number of cells.
    * @return the total number of cells.
    */
    public int getTotalCells(){
        return world.getWidth()*world.getHeight();
    }
    
    /**
    * Returns the total number of free cells.
    * By free, we mean not inaccessible.
    * @return the total number of free cells.
    */
    public int countFreeCells(){
        return getTotalCells()-countCells(Cell.Status.inaccessible);
    }

    /**
    * Returns the number of times the given Action has been called. If the simulation
    * is over and act==Action.noOp, noOp is subtracted the last series of noOps.
    * @param act the action to examine.
    * @return the number of times the given Action has been called.
    */
    public int getCalls(Action act){
        if(act==Action.noOp)
            return (inactiveSteps<2*agents.size() ? totalNoOps: totalNoOps-inactiveSteps);
        if(act==Action.nextMoveToTarget)
            return totalNextMoves;
        if(act==Action.nextStepInTask)
            return totalNextSteps;
        return -1;
    }

    /**
    * Returns the total number of steps performed. The number of total steps is
    * equal to getCalls(Action.noOp) + getCalls(Action.nextMoveToTarget) + getCalls(Action.nextStepInTask).
    * @return the number of total steps performed.
    */
    public int getTotalSteps(){
        return getCalls(Action.noOp)+totalNextMoves+totalNextSteps;
    }
    
    /**
    * Returns the total number of complete rounds the simulation has performed.
    * By performing a round, we mean doing one step for each agent.
    * The result is rounded by defect.
    * @return the number of total loops performed.
    */
    public int getTotalRounds(){
        return getTotalSteps()/agents.size();
    }
    
    /**
    * Returns the number of cells visited by the given agent.
    * @param agId the id of the agent
    */
    public int getVisited(int agId){
        int count=0;
        for(int row=world.getHeight()-1;row>=0;row--)
            for(int col=world.getWidth()-1;col>=0;col--)
                if(world.getCell(row,col).getStatus()==Cell.Status.visited&&world.getCell(row,col).getAgent()==agId)
                    count++;
        return count;
    }
    
    /**
    * Convenience method returning a String representation for all the statisics.
    * @return a String representation for the statisics.
    */
    public String statsToString(){
        StringBuilder builder=new StringBuilder();
        
        builder.append("Total cells: "+getTotalCells());
        builder.append("\nTotal free cells: "+countFreeCells());
        for(Cell.Status status : Cell.Status.values())
            builder.append("\nTotal "+status+" cells: "+countCells(status));
        
        builder.append("\n\nTotal rounds: "+getTotalRounds());
        builder.append("\nTotal steps: "+getTotalSteps());
        for(Action act : Action.values())
            builder.append("\nTotal "+act+" actions: "+getCalls(act));
        
        
        builder.append("\n");
        for(int agId=1;agId<=agents.size();agId++)
            builder.append("\nTotal cells explored by agent "+agId+": "+getVisited(agId));
            
        return builder.toString();
    }
}