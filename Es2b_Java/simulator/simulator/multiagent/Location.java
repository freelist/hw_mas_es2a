package multiagent;

import java.io.*;

/**
* This class represents a position in the world expressed as a couple.
* The coordinates are (row,column), the top-left Location being (0,0).
* An instance of this class can be used to hold an agent position.
* A task is denoted using the Location of its central Cell.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/
public class Location implements Serializable{
    
    /**
    * This class represents a direction. It's used to scan all possible Cells a given Location.
    * @version 1.0 - May 22, 2013
    * @author Federico Patota
    * @author Gabriele Buondonno
    */
    public static enum Direction{
        UP_LEFT,UP,UP_RIGHT,LEFT,CENTER,RIGHT,DOWN_LEFT,DOWN,DOWN_RIGHT
    }
    
    /**
    * The row coordinate.
    */
    public final int row;
    /**
    * The column coordinate.
    */
    public final int col;
    
    public Location(int row,int col){
        this.row=row;
        this.col=col;
    }
    
    /**
    * Given a direction, it returns a Location which is adjacent to this Location
    * in the given direction.
    * @param dir the direction.
    * @return the Location corresponding to the given direction.
    */
    public Location getLocation(Direction dir){
        int row;
        int col;
        
        if(dir==Direction.LEFT||dir==Direction.RIGHT||dir==Direction.CENTER)row=this.row;
        else if (dir==Direction.DOWN||dir==Direction.DOWN_LEFT||dir==Direction.DOWN_RIGHT)row=this.row+1;
        else row=this.row-1;
        
        if(dir==Direction.UP||dir==Direction.DOWN||dir==Direction.CENTER)col=this.col;
        else if (dir==Direction.RIGHT||dir==Direction.UP_RIGHT||dir==Direction.DOWN_RIGHT)col=this.col+1;
        else col=this.col-1;
        
        return new Location(row,col);
    }
    
    public boolean equals(Object o){
        if(o==null||o.getClass()!=getClass())return false;
        Location l=(Location)o;
        return row==l.row&&col==l.col;
    }
    
    /**
    * A String representation for this Location.
    */
    public String toString(){
        return "("+row+","+col+")";
    }
}
