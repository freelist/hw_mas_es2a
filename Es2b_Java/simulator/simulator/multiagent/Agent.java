package multiagent;

import java.util.*;
import java.io.*;

/**
* This class represents an agent. Each Agent is identified by an integer.
* <p> The Agent state is determined by:
* <p>  - task under execution;
* <p>  - action under execution;
* <p>  - pending tasks list: the tasks that have been discovered and not yet assigned to any Agent.
* <p> At each turn an Agent needs to: 
* <p>  - specify the next action;
* <p>  - based on the result of the action returned by the world the agent must update the task list;
* <p>  - try to allocate all the actions in the list;
* <p>  - release turn.
* <p> In addition, each Agent must act as provider when a request for Bid is issued by another agent.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/
public class Agent implements Serializable{
    /**Identifier for this agent.*/
    private final int id;
    
    /**current position of the agent.*/
    private Location position;
    
    /**task currently under execution by this agent.*/
    private Location currentTask;
    
    /**action currently under execution by this agent.*/
    private Action currentAction;
    
    /**Tasks discovered by this agent and not assigned to any agent yet.*/
    private LinkedList<Location> pendingTasks=new LinkedList<Location>();

    /**
    * Constructor for this class.
    * @param id identifier for this agent.
    * @param l initial agent position. 
    */
    public Agent(int id,Location l){
        this.id=id;
        position=l;
        currentAction=Action.noOp;
    }
    
    /**
    * Returns the agent identifier.
    * @return this agent identifier.
    */
    public int getId(){
        return id;
    }
    
    /**
    * Returns the current position of the agent.
    * @return the current position of this agent.
    */
    public Location getPosition(){
        return position;
    }

    /**
    * Returns the task currently under execution by the agent.
    * @return the task currently under execution by this agent.
    */
    public Location getCurrentTask(){
        return currentTask;
    }
    
    /**
    * Checks whether the pending tasks list is empty.
    * @return true if the pending tasks list is empty, otherwise false.
    */
    public boolean emptypTask(){
        return pendingTasks.isEmpty();
    }

    /**
    * Adds this location to the pending tasks list.
    */
    public void addpTask(Location l){
        pendingTasks.add(l);
    }

    /**
    * Removes this location from the pending tasks list.
    */
    public void removepTask(Location l){
        pendingTasks.remove(l);
    }
    
    /**
    * Returns a pending task from the pending tasks list, according to some priority.
    * If the list is empty, it returns null.
    * @return the next pending task to be auctioned.
    */
    public Location getNextpTask(){
        if(pendingTasks.isEmpty()) return null;
        return pendingTasks.get(0);
    }

    /**
    * The world calls this method to ask the agent for the next action to be executed.
    * The agent returns the action currently under execution.
    * @return the action that needs to be executed.
    */
    public Action nextAction(){
        return currentAction;
    }
    
    /**
    * This method is called by the simulation environment to inform
    * the agent about the outcome of its action, as provided by the World.
    * @param nextPosition the agent new position.
    * @param nextTask a new task discovered by this agent. If null, there is no next task.
    * If it's equal to Location(-1,-1), it acts as a flag to indicate the task is over.
    */
    public void updateState(Location nextPosition, Location nextTask){
    	Location flag = new Location(-1,-1);
    	if(this.currentTask == null){ 
    		this.currentAction = Action.noOp; 
    		return;
    		}
    	if(nextTask == null) this.currentAction = Action.noOp;
    	if(nextTask != null){
	    	if(nextTask.equals(flag)){
	    		this.currentAction = Action.noOp;
	    		this.cancelTask();  
	    		return;
	        }
	    	else{
	    		this.currentAction = Action.nextStepInTask;
	    		this.addpTask(nextTask);
	    	}
    	}
    	if(this.position != null){
	    	if(nextPosition != null){
		    	if(this.position.equals(nextPosition)){
		    		this.position = nextPosition;
		    		this.currentAction = Action.nextStepInTask;
		    		
		    	}
		    	if(!(this.position.equals(nextPosition))){
		    		this.position = nextPosition;
		    		this.currentAction = Action.nextMoveToTarget;
		    		if(nextTask!=null) this.addpTask(nextTask);
	
		    	}
	    	}
    	}

    	
    	
    }

    /** 
    * The requests for Bids are issued by the agent.
    * In the first implementation an agent can only bid if it has no current task.
    * @param task the task which the request is issued for.
    * @return a bid for the given task.
    */
    public Bid bidForTask(Location task){
    	Bid b;
    	int manhattan = -1;
    	if(this.currentTask == null){
	    	Location i = this.getPosition(); //actual position of the agent
	    	Location f = task;				//position of the task
	    	manhattan = Math.abs(i.col - f.col) + Math.abs(i.row - f.row); //evaluation of the manhattan distance
	    	b = new Bid(true,task,manhattan,this); //make the bid
			return b;
    	}
    	else return b = new Bid(false,task,manhattan,this);
    }
    
    /** The acceptance requests are issued by the agent.
    * In the first implementation an agent can only accept if it has no current task.
    * @param task the task which the request is issued for.
    * @return true if the task is accepted.
    */
    public boolean acceptTask(Location task){
    	if(this.currentTask == null){
    		this.currentTask = task;
    	
	    	if(this.position == task){
	    		this.currentAction = Action.nextStepInTask;
	    	}
	    	
	    	if(this.position != task){
	    		this.currentAction = Action.nextMoveToTarget;
	    		
	    	}
	    	return true;
    	} 
    	else return false;
    	
    }
    
    /**
    * Deletes current task setting it to null.
    */
    public void cancelTask(){
        this.currentTask = null;
        this.currentAction = Action.noOp;
    }
    
    /** 
    * The simulator asks the agent to allocate the pending tasks.
    * The tasks are immediately assigned to the best offerer.
    * The agent list is provided by the simulator.
    * @param agents the agents list.
    */
    public void assignTasks(List<Agent> agents){
    	Bid best = new Bid(false,null,100,null);	//initial best (cost = 100)
    	Iterator<Agent> agIt = agents.iterator();	//
    	while(this.getNextpTask() != null){			//while there are pending tasks...
    		if(agIt.hasNext() == false) break;		//...and there are available agents...
    		Agent a = agIt.next();					//...choose the agent...
    		Location bidTask = this.getNextpTask(); //...take the task...
    		Bid bid = a.bidForTask(bidTask);		//...make the bid...
    		if(bid.isPropose == true){				//...if the agent is not busy...
    			if(bid.cost <= best.cost) best = bid; //evaluate the offer
    			if(best.bidder.acceptTask(bid.task)) this.removepTask(bid.task);	
    		}
    		
    	}
    	
    }
}
