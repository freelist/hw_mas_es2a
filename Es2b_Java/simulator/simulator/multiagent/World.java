package multiagent;

import java.io.*;
import java.util.*;

/**
* This class represents the world as a grid of Cells and stores the agents Locations.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/
public class World{

    /**number of rows of the grid*/
    private int height;
    /**number of columns of the grid*/
    private int width;
    
    /**stores the state of the world*/
    private Cell[][] map;
    
    /**stores the  locations of the agents*/
    private List<Location>agents=new ArrayList<Location>();

    /**Simple constructor creating a world of size nxm without any inaccessible Cells*/
    public World(int m, int n) {
	height=m;
	width=n;
	map=new Cell[m][n];
	for (int r=0;r<m;r++)
            for (int c=0;c<n;c++)
                map[r][c]=new Cell();
    }


    /**
    * Adds inaccessible Cells.
    * @param n the number of inaccessible Cells that we want to add
    */
    public void addInaccessibleCells(int n) {
        if(n>=height*width)
            throw new IllegalArgumentException("Too many inaccessible cells");
        
        for(int i=n;i>0;i--){
            int row=(int)(height*Math.random());
            int col=(int)(width*Math.random());
            
            if(map[row][col].getStatus()!=Cell.Status.inaccessible)
                map[row][col].setStatus(Cell.Status.inaccessible);
            else
                i++;
        }
    }
    
    /**
    * Clears current agents and adds the positions  of the given ones to its own list.
    * @param agentList the list of the agents, ordered for id from 1 to agentList.size().
    */
    public void addAgents(List<Agent>agentList){
        agents=new ArrayList<Location>(agentList.size());
        for(Agent a : agentList)
            agents.add(a.getPosition());
    }


    // ******************************************** World management *************************

    /**
    * Records the grid map of the world in a file with the given name,
    * marking all non-inaccessible Cells as notSeen.
    * @param filename the name of the file the World will be stored
    * @throws IOException
    */
    public void storeWorld(String filename) throws IOException{
        World store=new World(height,width);
        for(int row=0;row<height;row++)
            for(int col=0;col<width;col++){
                Cell thisCell=getCell(row,col);
                if(thisCell.getStatus()==Cell.Status.inaccessible)
                    store.getCell(row,col).setStatus(Cell.Status.inaccessible);
            }
        new ObjectOutputStream(new FileOutputStream(new File(filename))).writeObject(store.map);
    }

    /**
    * Retrieves the world from a file with the given name.
    * @param filename the name of the file this World will be stored
    * @throws IOException
    */
    public static World retrieveWorld(String filename) throws IOException, ClassNotFoundException{
        World world=new World();
        Cell[][]grid=(Cell[][])(new ObjectInputStream(new FileInputStream(filename)).readObject());
        world.map=grid;
        world.height=grid.length;
        world.width=grid[0].length;
        return world;
    }
    
    private World(){}

    // ********************************************* Action Execution ***************************

    /**
    * Given an action, the agent is moved based on its task.
    * If the task is finished, location (-1,-1) is returned.
    * If the task is not finished, the agent looks for a new task to be allocated, and this value is returned.
    * If no new task is discovered, null is returned.
    * The agent internal state must be changed accordingly.
    * <p> When calling this method, the world is notified the fact that agent agId
    * has a task associated to agTask; so, it sets to agId the agent of all the underNeg
    * cells belonging to the task, if agTask is not already marked.
    * <p> An exception is thrown if act is Action.nextStepInTask and it's not applicable.
    * Action.nextStepInTask is considered applicable when the agent is in the task cell, or it 
    * lies in a cell belonging to the task and the central cell has already been visited.
    * @param act the action to be executed.
    * @param agId the id of the agent requesting to apply the action.
    * @param agTask the task of the agent requesting to apply the action.
    * @return location (-1,-1) if the task is finished. Otherwise, a new task (if no new task is discovered, null).
    * @throws NullPointerException if agTask or act are null.
    * @throws IllegalArgumentException if act is not applicable.
    */
    public Location executeAction(int agId, Location agTask, Action act){
        switch(act){
            case nextStepInTask:
                return executeActionExplore(agId,agTask);
            case nextMoveToTarget:
                return executeActionMove(agId,agTask);
            case noOp:
                return null;
            default:
                throw new IllegalArgumentException("Unknown action: "+act);
        }
    }
    
    /**
    * Auxiliary method for the execution of the nextMoveToTarget action
    * the agent is moved first vertically, then horizontally, skipping inaccessible cells.
    * @param agId the id of the agent requesting to apply the action.
    * @param agTask the task of the agent requesting to apply the action.
    * @return the location of the first notSeen cell in the lrud scan of the new position (if no new task is discovered, null).
    */
    private Location executeActionMove(int agId, Location agTask){
        markAgentTask(agId,agTask);
        Location pos=getAgentPosition(agId);

        do{
            if(agTask.row<pos.row){
                pos=pos.getLocation(Location.Direction.UP);
            }
            else if(agTask.row>pos.row){
                pos=pos.getLocation(Location.Direction.DOWN);
            }
            else if(agTask.col<pos.col){
                pos=pos.getLocation(Location.Direction.LEFT);
            }
            else if(agTask.col>pos.col){
                pos=pos.getLocation(Location.Direction.RIGHT);
            }
        }while(getCell(pos).getStatus()==Cell.Status.inaccessible);
        
        setAgentPosition(agId,pos);
        
        return scanNotSeen(pos);
    }

    /**
    * Auxiliary method for the execution of the exploration action (nextStepInTask).
    * A next cell to be explored is selected and marked as visited by agent agId.
    * If the central cell has not been visited yet and the agent is exactly
    * on it, this is the selected cell. Otherwise, the selected cell is
    * the first one in the lrud scan which belongs to its task and has not
    * been already visited. If no next cell exists, the agent task is achieved;
    * thus, new Location(-1,-1) is returned.
    * @param agId the id of the agent requesting to apply the action.
    * @param agTask the task of the agent requesting to apply the action.
    * @return location (-1,-1) if the task is finished. Otherwise, the location
    * of the first notSeen cell in the lrud scan of the new position, (if no new task is discovered, null).
    */
    private Location executeActionExplore(int agId, Location agTask){
        if(!isExploreApplicable(agId,agTask))
            throw new IllegalArgumentException("Agent "+agId+" cannot perform exploration for task "+agTask);
        markAgentTask(agId,agTask);
        Location newLoc=scanNextStep(agTask);
        if(newLoc!=null){
            setAgentPosition(agId,newLoc);
            getCell(newLoc).setStatus(Cell.Status.visited);
            getCell(newLoc).setAgent(agId);
            return scanNotSeen(newLoc);
        }
        else
            return new Location(-1,-1);
    }

    /**
    * Auxiliary method for checking whether Action.nextStepInTask is applicable for the given task.
    * The Action is considered applicable when the agent is in the task cell, or it 
    * lies in a cell belonging to the task and the central cell has already been visited.
    * @param agId the id of the agent requesting to apply the action.
    * @param agTask the task of the agent requesting to apply the action.
    * @return true if Action.nextStepInTask is applicable.
    */
    private boolean isExploreApplicable(int agId, Location agTask){
        Location pos=getAgentPosition(agId);
        Cell taskCell=getCell(agTask);
        return pos.equals(agTask)||agTask.equals(taskCell.getTask())&&taskCell.getStatus()==Cell.Status.visited&&taskCell.getAgent()==agId;
    }
    
    /**
    * This method, given a task, sets to agentId the agent of all the underNeg
    * cells belonging to the task.
    * The method immediaely returns if the central cell has already been marked,
    * so that it's never actually executed when not needed.
    * This method is called automatically whenever the world is asked
    * to execute an action for the given task.
    * @param agId the id of the agent requesting to apply the action.
    * @param agTask the task of the agent requesting to apply the action.
    */
    private void markAgentTask(int agId, Location agTask){
        if(getCell(agTask).getAgent()!=-1)
            return;
        Location.Direction[] dirs=Location.Direction.values();
        for(int i=0;i<dirs.length;i++){
            Location lnew=agTask.getLocation(dirs[i]);
            if(!isValid(lnew))continue;
            Cell cnew=getCell(lnew);
            if(cnew.getStatus()==Cell.Status.underNeg&&agTask.equals(cnew.getTask()))
                cnew.setAgent(agId);
        }
    }

    // ************************************************ Communication ****************************
    
    /**Location task and all its surroundings not already belonging to a task
    * are marked as underNeg and they are associated to the new task.
    * @param task the task to add.
    * @return true if task!=null&&isValid(task).
    */
    public boolean addTask(Location task) {
        if(task==null||!isValid(task))return false;
        while(true){
            Location loc=scanNotSeen(task);
            if(loc==null)return true;
            getCell(loc).setTask(task);
            getCell(loc).setStatus(Cell.Status.underNeg);
        }
    }

    /**
    * Returns true if there is an agent in the given Location.
    * @param pos the Location to check.
    * @return true if there is an agent in Location pos
    */
    public boolean isAgent(Location pos) {
	for(Location loc : agents)
            if(loc.equals(pos))
                return true;
	return false;
    }
    
    /**
    * Returns true if there is an agent in the given coordinates.
    * @param row the row of the Location to check.
    * @param col the column of the Location to check.
    * @return true if there is an agent in Location (row,col).
    */
    public boolean isAgent(int row, int col){
         return isAgent(new Location(row,col));
    }
    
    /**
    * Returns all the agents in the given Location.
    * @param loc the Location to check.
    * @return list of all the id's of the agents in Location loc.
    */
    public List<Integer> getAgents(Location loc){
        java.util.Iterator<Location>it=agents.listIterator();
        List<Integer>res=new LinkedList<Integer>();
        for(int id=1;it.hasNext();id++)
            if(it.next().equals(loc))
                res.add(id);
        return res;
    }
    
    /**
    * Returns all the agents in the Location with given coordinates.
    * @param row the row of the Location to check.
    * @param col the column of the Location to check.
    * @return list of all the id's of the agents in Location (row,col).
    */
    public List<Integer> getAgents(int row, int col){
        return getAgents(new Location(row,col));
    }
    
    
    /**
    * Returns an iterator of the agents list.
    * @return iterator of the Locations of all the agents.
    */
    public ListIterator<Location> getAgentIterator(){
         return agents.listIterator();
    }
    
    // ************************************************* Selectors to access the world components *******************

    /**Returns the number of rows of the world.
    * @return the number of rows of the world.
    */
    public int getHeight(){
        return height;
    }
    
    /**Returns the number of columns of the world.
    * @return the number of columns of the world.
    */
    public int getWidth(){
        return width;
    }
    
    /**Returns the number of agents in the world.
    * @return the number of agents in the world.
    */
    public int getNumAgents(){
        return agents.size();
    }
    
    /**
    * Returns the Cell with given coordinates.
    * @param row the row of the Cell.
    * @param col the column of the Cell.
    * @return the Cell with given coordinates.
    */
    public Cell getCell(int row, int col){
        return map[row][col];
    }
    
    /**
    * Returns the Cell with coordinates corresponding to loc.
    * @param loc the Location of the Cell.
    * @return the Cell with coordinates corresponding to loc.
    */
    public Cell getCell(Location loc){
        return map[loc.row][loc.col];
    }
    
    /**
    * Returns an Agent position.
    * @param id the id of the agent.
    * @return the Location corresponding to the Agent position in the world.
    */
    public Location getAgentPosition(int id){
        return agents.get(id-1);
    }
    
    /**
    * Sets an Agent position.
    * @param id the id of the agent.
    * @param loc the Location corresponding to the new Agent position in the world.
    */
    public void setAgentPosition(int id,Location loc){
        agents.set(id-1,loc);
    }
    
    /**
    * Returns true if the given location falls within the grid.
    * @param loc the Location to check.
    * @return true if the given location falls within the grid.
    */
    public boolean isValid(Location loc){
        return loc.row>=0&&loc.row<height&&loc.col>=0&&loc.col<width;
    }
    
    /**
    * Scans for all not seen cells surrounding the given Location.
    * @param loc the Location to scan around.
    * @return the first notSeen Cell around loc.
    */
    public Location scanNotSeen(Location loc){
        Location.Direction[] dirs=Location.Direction.values();
        for(int i=0;i<dirs.length;i++){
            Location lnew=loc.getLocation(dirs[i]);
            if(isValid(lnew)&&(getCell(lnew).getStatus()==Cell.Status.notSeen))
                return lnew;
        }
        return null;
    }
    
    /**
    * Scans for all underNeg cells surrounding the given Location.
    * @param loc the Location to scan around.
    * @return the first underNeg Cell belonging to the same task as loc.
    */
    public Location scanNextStep(Location loc){
        if(getCell(loc).getStatus()==Cell.Status.underNeg&&getCell(loc).getTask().equals(loc))
            return loc;
        Location.Direction[] dirs=Location.Direction.values();
        for(int i=0;i<dirs.length;i++){
            Location lnew=loc.getLocation(dirs[i]);
            if(isValid(lnew)&&getCell(lnew).getStatus()==Cell.Status.underNeg&&getCell(lnew).getTask().equals(loc))
                return lnew;
        }
        return null;
    }
}
