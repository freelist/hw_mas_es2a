package multiagent;

import java.io.*;
import java.util.*;

/**
* This class represents a cell of the World.
* @version 1.0 - May 22, 2013
* @author Federico Patota
* @author Gabriele Buondonno
*/
public class Cell implements Serializable{

    /**
    * This class represents a possible Status a Cell can have.
    * @version 1.0 - May 22, 2013
    * @author Federico Patota
    * @author Gabriele Buondonno
    */
    public static enum Status{
        /**
        * The cell has been visited by an agent.
        */
        visited,
        /**
        * The cell has been spotted but no agent has visited it yet (under negotiation).
        */
        underNeg,
        /**
        * The cell has not been seen yet.
        */
        notSeen,
        /**
        * The cell is not accessible. This status cannot be changed.
        */
        inaccessible
    }

    /**The Status of the Cell.*/
    private Status status;
    
    /**
    * The Agent that has visited this Cell or that has requested to execute
    * an action relative to this cell (-1 if there is no agent).
    */
    private int agent=-1;
    
    /**Location of the task this Cell is associated to (initially null)*/
    private Location task;
    
    /**
    * Creates a new Cell with Status notSeen.
    */
    public Cell(){
        this.status=Status.notSeen;
    }

    /**
    * Returns the Agent associated to this Cell.
    * The Agent associated to this cell is the one that has visited it or that
    * has requested to execute an action relative to this cell. If there is no
    * such agent, -1 is returned.
    * @return the Agent associated to this Cell. If there is no such agent, -1.
    */
    public int getAgent(){
        return agent;
    }
    
    /**
    * Returns the Status of the Cell.
    * @return the Status of this Cell.
    */
    public Status getStatus(){
        return status;
    }
    
    /**
    * Returns the Location of the task the Cell is associated to.
    * @return the Location of the task this Cell is associated to. If this Cell is not associated to any task, null.
    */
    public Location getTask(){
        return task;
    }
    
    /**
    * Sets the Agent associated to this Cell.
    * The Agent associated to this cell is the one that has visited it or that
    * has requested to execute an action relative to this cell (-1 if there is no agent).
    * @throws IllegalArgumentException if this Cell is either inaccessible or notSeen.
    */
    public void setAgent(int ag){
        if(status==Status.notSeen||status==Status.inaccessible)
            throw new IllegalArgumentException("Agent cannot be set for "+status+" cells");
        agent=ag;
    }
    
    /**
    * This method sets the Status of this Cell.
    * @throws NullPointerException if the given Status is null.
    * @throws IllegalArgumentException if this Cell is inaccessible (inaccessible Cells cannot change Status).
    */
    public void setStatus(Status st){
        if(st==null)
            throw new NullPointerException("Cell status cannot be null");
        if(status==Status.inaccessible)
            throw new IllegalArgumentException("Inaccessible cells cannot change status");
        status=st;
    }
    
    /**
    * This method sets the task this Cell is associated to.
    * @throws NullPointerException if the given task is null.
    * @throws IllegalArgumentException if there already exists a task for this Cell.
    */
    public void setTask(Location task){
        if(task==null)
            throw new NullPointerException("Trying to assing a null task to a Cell");
        if(this.task!=null)
            throw new IllegalArgumentException("Trying to change task a Cell belongs to");
        this.task=task;
    }
}