function [ Attacker, Defender ] = initializeAgentPositions()
    %posX,posY,type,Id,name,maxMovement,taskAssignment,listTask
	attacker1 = Agent(9,5,1,1,'Predator1',1,false,[]);
	attacker2 = Agent(2,9,1,2,'Predator2',1,false,[]);
	attacker3 = Agent(5,9,1,3,'Predator3',1,false,[]);
	%attacker4 = Agent(7,6,1,4,'Predator4',1,false,[]);
	%attacker5 = Agent(2,2,1,5,'Predator5',1,false,[]);
	
	defender1 = Agent(1,4,2,1,'Prey1',1,false,[]);
	defender2 = Agent(6,3,2,2,'Prey2',1,false,[]);
	defender3 = Agent(9,2,2,3,'Prey3',1,false,[]);
	defender4 = Agent(3,1,2,4,'Prey4',1,false,[]);
	defender5 = Agent(4,7,2,5,'Prey5',1,false,[]);
	
	Attacker = [ attacker1, attacker2, attacker3 ];
	Defender = [ defender1, defender2, defender3, defender4, defender5 ];
end
