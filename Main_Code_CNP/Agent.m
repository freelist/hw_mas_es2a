classdef Agent
	properties
        %%
		POS_X
		POS_Y
        maxMovement
        max
        type
        id
        name
        taskAssigned
        tasks
        isDead
        ctrDeath
        ctrDeathPerPredators
        %%
        outMsgBuffer
        inMsgBuffer
        inAssignmentBuffer %buffer for assignment messages, because they have priority
        listToResend
	end
	
	methods
        %%
		function obj = Agent(POS_X, POS_Y, TYPE, ID, NAME, MAXMOVEMENT, TASKASSIGNED, TASKS)
			obj.POS_X = POS_X;
			obj.POS_Y = POS_Y;
            obj.type = TYPE;
            obj.id = ID;
            obj.name = NAME;
            obj.taskAssigned = TASKASSIGNED;
            obj.tasks = TASKS;
            obj.isDead = false;
            obj.maxMovement = MAXMOVEMENT;
            obj.outMsgBuffer = {};
            obj.inMsgBuffer = {};
            obj.inAssignmentBuffer = {};
            obj.listToResend = [];
            obj.ctrDeath = 0;
            obj.ctrDeathPerPredators = [0,0,0,0,0];
		end
		%%
		function [ obj, worldObj ] = move(obj, worldObj, X, Y)
            [ Res , worldObj ] = worldObj.tryMove(obj.POS_X,obj.POS_Y,X,Y);
            if Res == 1
                obj.POS_X = X;
                obj.POS_Y = Y;
            end
        end
        %%
        function [goal_x, goal_y] = calculateGoal(obj, worldObj)
            prey_x = worldObj.Defender(obj.tasks(end)).POS_X;
            prey_y = worldObj.Defender(obj.tasks(end)).POS_Y;
            cellUp = [prey_x-1,prey_y]; 
            if ((cellUp(1,1)<1 ||cellUp(1,2)<1) || (cellUp(1,1)>worldObj.DIM_X || cellUp(1,2)>worldObj.DIM_Y))
                cellUp = [0,0];
            end
            cellDown = [prey_x+1,prey_y];
            if ((cellDown(1,1)<1 ||cellDown(1,2)<1) || (cellDown(1,1)>worldObj.DIM_X || cellDown(1,2)>worldObj.DIM_Y))
                cellDown = [0,0];
            end
            cellLeft = [prey_x,prey_y-1];
            if ((cellLeft(1,1)<1 ||cellLeft(1,2)<1) || (cellLeft(1,1)>worldObj.DIM_X || cellLeft(1,2)>worldObj.DIM_Y))
                cellLeft = [0,0];
            end
            cellRight = [prey_x,prey_y+1];
            if ((cellRight(1,1)<1 ||cellRight(1,2)<1) || (cellRight(1,1)>worldObj.DIM_X || cellRight(1,2)>worldObj.DIM_Y))
                cellRight = [0,0];
            end
            cellUpLeft = [prey_x-1,prey_y-1];
            if ((cellUpLeft(1,1)<1 ||cellUpLeft(1,2)<1) || (cellUpLeft(1,1)>worldObj.DIM_X || cellUpLeft(1,2)>worldObj.DIM_Y))
                cellUpLeft = [0,0];
            end
            cellUpRight = [prey_x-1,prey_y+1];
            if ((cellUpRight(1,1)<1 ||cellUpRight(1,2)<1) || (cellUpRight(1,1)>worldObj.DIM_X || cellUpRight(1,2)>worldObj.DIM_Y))
                cellUpRight = [0,0];
            end
            cellBottomLeft = [prey_x+1,prey_y-1];
            if ((cellBottomLeft(1,1)<1 ||cellBottomLeft(1,2)<1) || (cellBottomLeft(1,1)>worldObj.DIM_X || cellBottomLeft(1,2)>worldObj.DIM_Y))
                cellBottomLeft = [0,0];
            end
            cellBottomRight = [prey_x+1,prey_y+1];
            if ((cellBottomRight(1,1)<1 ||cellBottomRight(1,2)<1) || (cellBottomRight(1,1)>worldObj.DIM_X || cellBottomRight(1,2)>worldObj.DIM_Y))
                cellBottomRight = [0,0];
            end
            adiacentCells = [cellUp;cellDown;cellLeft;cellRight;cellUpLeft;cellUpRight;cellBottomLeft;cellBottomRight];
            realAdiacentCells = [];
            ctr=1;
            for i=1:8
                if adiacentCells(i,1)~=0 && adiacentCells(i,2)~=0
                    realAdiacentCells(ctr,:) = adiacentCells(i,:);
                    ctr = ctr+1;
                end
            end
            r = randi([1,length(realAdiacentCells)]);
            goal_x = adiacentCells(r,1);
            goal_y = adiacentCells(r,2);
        end
        %%
        function [d] = calculateD(obj, goal_x, goal_y)
            if (abs(obj.POS_X-goal_x)<=1 && abs(obj.POS_Y-goal_y)<=1)
                d = 1;
            else
                d = 0;
            end
        end
        %%
        function [res_x, res_y, moveFlag] = nextStep(obj, worldObj)
            moveFlag = true;
            if obj.taskAssigned
                [goal_x, goal_y] = obj.calculateGoal(worldObj);
                md = obj.calculateD(worldObj.Defender(obj.tasks(1)).POS_X,worldObj.Defender(obj.tasks(1)).POS_Y);
                if md == 1
                    moveFlag = false;
                    res_x = goal_x;
                    res_y = goal_y;
                else
                    goal_x_reached = false;
                    goal_y_reached = false;
                    %decide where to move up/down or right/left
                    if obj.POS_X == goal_x
                        goal_x_reached = true;
                    end
                    if obj.POS_Y == goal_y
                        goal_y_reached = true;
                    end
                    if goal_x_reached
                        movementChoice = 2;
                    elseif goal_y_reached
                        movementChoice = 1;
                    else
                        movementChoice = randi([1,2]);
                    end
                    if movementChoice == 1
                        % vai giu
                        if (goal_x - obj.POS_X) > 0
                            NEW_X = obj.POS_X + 1;
                            NEW_Y = obj.POS_Y;
                            % check boundary
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                %vado a dx
                                if (goal_y - obj.POS_Y) > 0
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y + 1;
                                    if ((NEW_X2<1 ||NEW_Y<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 - 2;
                                        if ((res_x<1 ||res_y<1) || (res_x>worldObj.DIM_X || res_y>worldObj.DIM_Y))
                                            res_x = NEW_X2;
                                            res_y = NEW_Y2 - 1;
                                        else
                                            res_x = NEW_X2;
                                            res_y = NEW_Y2 - 2;
                                        end
                                    end
                                % vado a sx    
                                else
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y - 1;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 + 1;
                                    end
                                end
                            end
                        % vai su
                        else
                            NEW_X = obj.POS_X - 1;
                            NEW_Y = obj.POS_Y;
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                % vado a dx
                                if (goal_y - obj.POS_Y) > 0
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y + 1;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 - 1;
                                    end
                                % vado a sx
                                else
                                    NEW_X2 = obj.POS_X;
                                    NEW_Y2 = obj.POS_Y - 1;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2 + 1;
                                    end
                                end
                            end
                        end
                    elseif movementChoice == 2
                        % vai a dx
                        if (goal_y - obj.POS_Y) > 0
                            NEW_X = obj.POS_X;
                            NEW_Y = obj.POS_Y + 1;
                            % check boundary
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                %vado su
                                if (goal_x - obj.POS_X) > 0
                                    NEW_X2 = obj.POS_X - 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 + 2;
                                        res_y = NEW_Y2;
                                        if ((res_x<1 ||res_y<1) || (res_x>worldObj.DIM_X || res_y>worldObj.DIM_Y))
                                            res_x = NEW_X2 + 1;
                                            res_y = NEW_Y2;
                                        else
                                            res_x = NEW_X2 + 2;
                                            res_y = NEW_Y2;
                                        end
                                    end
                                % vado giu    
                                else
                                    NEW_X2 = obj.POS_X + 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 - 1;
                                        res_y = NEW_Y2;
                                    end
                                end
                            end
                        % vado a sx
                        else
                            NEW_X = obj.POS_X;
                            NEW_Y = obj.POS_Y - 1;
                            if ((NEW_X<1 ||NEW_Y<1) || (NEW_X>worldObj.DIM_X || NEW_Y>worldObj.DIM_Y))
                               isFreeCell = 0;
                            else
                               isFreeCell = worldObj.isFree(NEW_X,NEW_Y); 
                            end
                            if isFreeCell == 1
                                res_x = NEW_X;
                                res_y = NEW_Y;
                            else
                                % vado giu
                                if (goal_x - obj.POS_X) > 0
                                    NEW_X2 = obj.POS_X + 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 - 1;
                                        res_y = NEW_Y2;
                                    end
                                % vado su
                                else
                                    NEW_X2 = obj.POS_X - 1;
                                    NEW_Y2 = obj.POS_Y;
                                    if ((NEW_X2<1 ||NEW_Y2<1) || (NEW_X2>worldObj.DIM_X || NEW_Y2>worldObj.DIM_Y))
                                       isFreeCell2 = 0;
                                    else
                                       isFreeCell2 = worldObj.isFree(NEW_X2,NEW_Y2); 
                                    end
                                    if isFreeCell2 == 1
                                        res_x = NEW_X2;
                                        res_y = NEW_Y2;
                                    else
                                        res_x = NEW_X2 + 1;
                                        res_y = NEW_Y2;
                                    end
                                end
                            end
                        end
                    end
                end
            else
                moveChoice = randi([1 4]);
                if moveChoice == 1 %go up
                    if ((obj.POS_X - obj.maxMovement) > 0)
                        res_x = obj.POS_X - obj.maxMovement;
                        res_y = obj.POS_Y;
                    else
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y;
                    end
                elseif moveChoice == 2 %go right
                    if ((obj.POS_Y + obj.maxMovement) <= worldObj.DIM_Y)
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y + obj.maxMovement;
                    else
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y;
                    end
                elseif moveChoice == 3 %go down
                    if ((obj.POS_X + obj.maxMovement) <= worldObj.DIM_X)
                        res_x = obj.POS_X + obj.maxMovement;
                        res_y = obj.POS_Y;
                    else
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y;
                    end
                else %go left
                    if ((obj.POS_Y - obj.maxMovement) > 0)
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y - obj.maxMovement;
                    else
                        res_x = obj.POS_X;
                        res_y = obj.POS_Y;
                    end
                end
%                 movementChoice = randi([1,2]);
%                 if movementChoice == 1
%                     UpOrDown = randi(2);
%                     if (UpOrDown==1) && ((obj.POS_X + obj.maxMovement) <= worldObj.DIM_X)
%                         res_x = obj.POS_X + obj.maxMovement;
%                         res_y = obj.POS_Y;
%                     else
%                         if((obj.POS_X - obj.maxMovement) > 0)
%                             res_x = obj.POS_X - obj.maxMovement;
%                             res_y = obj.POS_Y;
%                         else
%                             res_x = obj.POS_X;
%                             res_y = obj.POS_Y;
%                         end
%                     end
%                 else
%                     DxOrSx = randi(2);
%                     if (DxOrSx==1) && ((obj.POS_Y + obj.maxMovement) <= worldObj.DIM_Y)
%                         res_x = obj.POS_X;
%                         res_y = obj.POS_Y + obj.maxMovement;
%                     else
%                         if((obj.POS_Y - obj.maxMovement) > 0)
%                             res_x = obj.POS_X;
%                             res_y = obj.POS_Y - obj.maxMovement;
%                         else
%                             res_x = obj.POS_X;
%                             res_y = obj.POS_Y;
%                         end
%                     end
%                 end
            end
        end
        %%
        function [cost] = computeUtilityF(obj,worldObj,preyID)
            cost= 1-(abs(obj.POS_X-worldObj.Defender(preyID).POS_X)+abs(obj.POS_Y-worldObj.Defender(preyID).POS_Y))/(worldObj.DIM_X + worldObj.DIM_Y);        
        end
        %%
        function [Attackers] = evaluateRequests(obj,Attackers,world)
            %first read all the assignment
            for i=1:length(obj.inAssignmentBuffer)
                if obj.inAssignmentBuffer{i}.readFlag == false
                    if obj.inAssignmentBuffer{i}.type == 2
                        if isempty(obj.tasks)
                            Attackers(obj.id).taskAssigned = true;
                            Attackers(obj.id).tasks(end+1) = obj.inAssignmentBuffer{i}.value;
                            Attackers(obj.id).inAssignmentBuffer{i}.readFlag = true;
                        else
                            if(isempty(find(obj.tasks==obj.inAssignmentBuffer{i}.value)))
                                Attackers(obj.id).taskAssigned = true;
                                Attackers(obj.id).tasks(end+1) = obj.inAssignmentBuffer{i}.value;
                                Attackers(obj.id).inAssignmentBuffer{i}.readFlag = true;
                            end
                        end
                    end
                end
            end
            %after read all the other messages
            for i=1:length(obj.inMsgBuffer)
                if obj.inMsgBuffer{i}.readFlag == false
                    if obj.inMsgBuffer{i}.type == 0
                        %it is a broadcast message
                        if(obj.taskAssigned == false)
                            %I can answer with my cost for that prey
                            msgValue = obj.computeUtilityF(world,obj.inMsgBuffer{i}.value);
                        else
                            %-1 for busy agent response
                            msgValue = -1;
                        end
                        %send the message
                        msg = Message(1,msgValue,obj.inMsgBuffer{i}.tag,obj.id);
                        Attackers(obj.id).outMsgBuffer{end+1} = msg;
                        Attackers(obj.inMsgBuffer{i}.sender).inMsgBuffer{end+1} = msg;
                    elseif obj.inMsgBuffer{i}.type == 1
                        %need to find all the agents that answered my
                        %message == have the same tag in the msg
                        msgWithSameTag = {};
                        %msgWithSameTag{end+1} = obj.inMsgBuffer{i};
                        for h=i:length(obj.inMsgBuffer)
                            if(obj.inMsgBuffer{h}.readFlag == false && obj.inMsgBuffer{h}.value>0 && obj.inMsgBuffer{h}.type == 1 && obj.inMsgBuffer{h}.tag==obj.inMsgBuffer{i}.tag)
                                msgWithSameTag{end+1} = obj.inMsgBuffer{h};
                                obj.inMsgBuffer{h}.readFlag = true;
                                Attackers(obj.id).inMsgBuffer{h}.readFlag = true;
                            end
                        end
                        if length(msgWithSameTag)>0
                            %evaluate the candidate
                            max = -1;
                            predatorAssigned = msgWithSameTag{1}.sender;
                            for t=1:length(msgWithSameTag)
                                if msgWithSameTag{t}.value>max
                                    max = msgWithSameTag{t}.value;
                                    predatorAssigned = msgWithSameTag{t}.sender;
                                end
                            end
                            for u=1:length(obj.outMsgBuffer)
                                if(obj.outMsgBuffer{u}.type == 0 && obj.outMsgBuffer{u}.tag == msgWithSameTag{1}.tag)
                                    msgValue = obj.outMsgBuffer{u}.value;
                                end
                            end
                            if max~=-1
                                %send the message with the assignment
                                msg = Message(2,msgValue,msgWithSameTag{1}.tag,obj.id);
                                Attackers(obj.id).outMsgBuffer{end+1} = msg;
                                Attackers(predatorAssigned).inAssignmentBuffer{end+1} = msg;
                                display(['predator',num2str(obj.id),' assigned Prey',num2str(msgValue),' to Predator',num2str(predatorAssigned)]);
                            else
                                if(isempty(find(Attackers(obj.id).listToResend==msgValue)))
                                    Attackers(obj.id).listToResend(end+1) = msgValue;
                                end
                            end
                        end
                    %elseif obj.inMsgBuffer{i}.type == 2
                    %    if(find(obj.tasks==obj.inMsgBuffer{i}.value)==0)
                    %        Attackers(obj.id).taskAssigned = true;
                    %        Attackers(obj.id).tasks(end+1) = obj.inMsgBuffer{i}.value;
                    %    end
                    end
                    %the message has been read and evaluated
                    Attackers(obj.id).inMsgBuffer{i}.readFlag = true;
                end
            end
        end
        %%
        function [Attackers] = requestHelp(obj,idPrey,Attackers)
            msg = Message(0,idPrey,length(obj.outMsgBuffer)+1,obj.id);
            for i=1:length(Attackers)
                if(Attackers(i).id~=obj.id)
                    Attackers(i).inMsgBuffer{end+1} = msg;
                else
                    Attackers(i).outMsgBuffer{end+1} = msg;
                end
            end
        end
        %%
        function Attackers = answerHelps(obj,Attackers)
            
        end
        %%
        function [Res, DetectedPreys] = detectPrey(obj, worldObj)
            Res = 0;
            DetectedPreys = [];
            counter = 1;
            Defender = worldObj.Defender;
            cellUp = [obj.POS_X-1,obj.POS_Y];
            cellDown = [obj.POS_X+1,obj.POS_Y];
            cellLeft = [obj.POS_X,obj.POS_Y-1];
            cellRight = [obj.POS_X,obj.POS_Y+1];
            cellUpLeft = [obj.POS_X-1,obj.POS_Y-1];
            cellUpRight = [obj.POS_X-1,obj.POS_Y+1];
            cellBottomLeft = [obj.POS_X+1,obj.POS_Y-1];
            cellBottomRight = [obj.POS_X+1,obj.POS_Y+1];
            for i=1:length(Defender)
                if Defender(i).isDead ~= true
                    if Defender(i).POS_X == cellUp(1,1) && Defender(i).POS_Y == cellUp(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellDown(1,1) && Defender(i).POS_Y == cellDown(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellLeft(1,1) && Defender(i).POS_Y == cellLeft(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellRight(1,1) && Defender(i).POS_Y == cellRight(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellUpLeft(1,1) && Defender(i).POS_Y == cellUpLeft(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellUpRight(1,1) && Defender(i).POS_Y == cellUpRight(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellBottomLeft(1,1) && Defender(i).POS_Y == cellBottomLeft(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                    if Defender(i).POS_X == cellBottomRight(1,1) && Defender(i).POS_Y == cellBottomRight(1,2)
                        Res = 1;
                        DetectedPreys(counter) = Defender(i).id;
                        counter = counter + 1;
                    end
                end
            end
        end
	end
end
