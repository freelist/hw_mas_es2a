classdef Message
	properties
        type    %0:BROADCAST;1:ANSWER;2:ASSIGNMENT
        value   %ID_PREY;COST;ID_PREY
        tag     %[idSender,idReceiver,type]
        readFlag
        sender
	end
	
	methods
        function obj = Message(TYPE,VALUE,TAG,SENDER)
            obj.type = TYPE;
            obj.tag = TAG;
            obj.value = VALUE;
            obj.readFlag = false;
            obj.sender = SENDER;
		end
    end
end