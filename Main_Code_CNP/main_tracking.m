function main_tracking
	DEBUG = 0;
	
	[ Attacker, Defender ] = initializeAgentPositions;
	
	world = World(10,10);
	
	[ world ] = world.initWorld(Attacker,Defender);
	
	figure('name','World');
	
	world.printWorldState(world.Attacker,world.Defender);
    %pause
	
	isGameEnded = 0;
    
    stepCtr = 1;
	
	while isGameEnded == 0
		
        display(['************** STEP ',num2str(stepCtr),' **************']);
        stepCtr = stepCtr + 1;
        
        for i = 1:length(Attacker)
            %first evaluate all the previous requests
            Attacker = Attacker(i).evaluateRequests(Attacker,world);
            world.Attacker = Attacker;
            %then detect the preys before moving
            [DetectionFlag, DetectedPreys] = Attacker(i).detectPrey(world);
            if DetectionFlag==1
                if(Attacker(i).taskAssigned==false)
                    for j=1:length(DetectedPreys)
                        %ask for help for the detected prey
                        Attacker = Attacker(i).requestHelp(DetectedPreys(j),Attacker);
                        Attacker(i).taskAssigned = true;
                        Attacker(i).tasks(end+1) = DetectedPreys(j);
                        %Defender(DetectedPreys(j)).ctrDeath = Defender(DetectedPreys(j)).ctrDeath + 1;
                        %world.Defender(DetectedPreys(j)).ctrDeath = world.Defender(DetectedPreys(j)).ctrDeath + 1;
                        world.Attacker = Attacker;
                        %display(['ctrDeath preda ',num2str(world.Defender(DetectedPreys(j)).id),': ',num2str(world.Defender(DetectedPreys(j)).ctrDeath)]);
                    end
                else
                    for j=1:length(DetectedPreys)
                        flagDeath = false;
                        for k=1:length(Attacker(i).tasks)
                            if Attacker(i).tasks(k) == DetectedPreys(j)
                                flagDeath = true;
                            end
                        end
                        if (flagDeath) && (Defender(DetectedPreys(j)).ctrDeathPerPredators(1,Attacker(i).id)<1)
                            Defender(DetectedPreys(j)).ctrDeath = Defender(DetectedPreys(j)).ctrDeath + 1;
                            Defender(DetectedPreys(j)).ctrDeathPerPredators(1,Attacker(i).id) = Defender(DetectedPreys(j)).ctrDeathPerPredators(1,Attacker(i).id) + 1;
                        else
                            %check if I already sent a help request for
                            %that prey
                            flagRequest = true;
                            for y=1:length(Attacker(i).outMsgBuffer)
                                if(Attacker(i).outMsgBuffer{y}.type==0 && Attacker(i).outMsgBuffer{y}.value==DetectedPreys(j) && isempty(find(Attacker(i).listToResend==DetectedPreys(j))))
                                    flagRequest = false;
                                end
                            end
                            if ~isempty(find(Attacker(i).listToResend==DetectedPreys(j)))
                                Attacker = Attacker(i).requestHelp(DetectedPreys(j),Attacker);
                                Attacker(i).listToResend = Attacker(i).listToResend~=DetectedPreys(j);
                                world.Attacker = Attacker;
                            end
                            if flagRequest
                                Attacker = Attacker(i).requestHelp(DetectedPreys(j),Attacker);
                                world.Attacker = Attacker;
                            end
                        end
                    end
                end
            end
            %evaluate next step
            [r_x, r_y, moveFlag] = Attacker(i).nextStep(world);
            if(moveFlag)
                %move towards the r_x r_y
                [ Attacker(i), world ] = Attacker(i).move(world,r_x,r_y);
            end
        end
        
        for i = 1:length(Defender)
            if Defender(i).isDead ~= true
                if(Defender(i).ctrDeath == 2)
                    for j = 1:length(Attacker)
                        if(find(Attacker(j).tasks==Defender(i).id)>0)
                            if(length(Attacker(j).tasks)-1 ~= 0 )
                                Attacker(j).tasks = Attacker(j).tasks(Attacker(j).tasks~=Defender(i).id);
                                world.Attacker(j).tasks = world.Attacker(j).tasks(world.Attacker(j).tasks~=Defender(i).id);
                            else
                                Attacker(j).tasks = [];
                                world.Attacker(j).tasks = [];
                                Attacker(j).taskAssigned = false;
                                world.Attacker(j).taskAssigned = false;
                            end
                        end
                    end
                    [world, Defender]= world.deletePrey(Defender(i).id,Defender);
                end
            end
        end
		
		world.printWorldState(Attacker,Defender);
        
        ctrEnd = 0;
        for i=1:length(Defender)
            if Defender(i).isDead
                ctrEnd = ctrEnd + 1;
            end
        end
        
        if ctrEnd == length(Defender)
            isGameEnded = 1;
            fprintf('\n*************************************************************\n');
            fprintf('GAME ENDED.');
            fprintf('*************************************************************\n\n');
        end
		
		pause(0.5);
	end
end
