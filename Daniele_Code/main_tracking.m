function main_tracking
	DEBUG = 0;
	
	[ Attacker, Defender ] = initializeAgentPositions;
	
	world = World(10,10);
	
	[ world ] = world.initWorld(Attacker,Defender);
	
	figure('name','World');
	
	world.printWorldState(world.Attacker,world.Defender);
    %pause
	
	if DEBUG == 1
		fprintf('\n');
	end
	
	isGameEnded = 0;
	
	while isGameEnded == 0
		for i = 1:length(Attacker(1,:))
			%r_x = randi(world.DIM_X);
			%r_y = randi(world.DIM_Y);
            [r_x, r_y, moveFlag] = Attacker(i).nextStep(world);
			
            if(moveFlag)
                [ Res, Attacker(i), world, DetectedPraysFlag, DetectedPrays ] = Attacker(i).move(world,r_x,r_y);
            else
                %just for debug
                %[world, Defender]= world.deletePrey(Attacker(i).tasks(end),Defender);
                if(length(Attacker(i).tasks)-1 ~= 0)
                    Attacker(i).tasks = Attacker(i).tasks(1:(end-1));
                end
                Res = 1;
            end
			
			if Res == 2
				isGameEnded = 1;
				
				fprintf('\n*************************************************************\n');
				fprintf('GAME ENDED.');
				fprintf('*************************************************************\n\n');
			end
			
			if DEBUG == 1
				fprintf('Attacker %d is now in state: (%d, %d)\n', i, Attacker(i).POS_X, Attacker(i).POS_Y);
			end
		end
		
		if DEBUG == 1
			fprintf('\n');
		end
		
		%for i = 1:length(Defender(1,:))
		%	r_x = randi(world.DIM_X);
		%	r_y = randi(world.DIM_Y);
		%	
		%	[ ~, Defender(i), world ] = Defender(i).move(world,r_x,r_y);
		%	
		%	if DEBUG == 1
		%		fprintf('Defender %d is now in state: (%d, %d)\n', i, Defender(i).POS_X, Defender(i).POS_Y);
		%	end
		%end
		
		if DEBUG == 1
			fprintf('\n');
		end
		
		world.printWorldState(Attacker,Defender);
		
		if DEBUG == 1
			fprintf('\n');
		end
		
		pause(0.3);
	end
end
